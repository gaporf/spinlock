//
// Created by gaporf on 30.03.2020.
//

#pragma once

#include <atomic>

struct spinlock {
    spinlock();

    spinlock(spinlock const &rhs) = delete;

    spinlock &operator=(spinlock const &rhs) = delete;

    ~spinlock() = default;

    void lock();

    void unlock();

    bool try_lock();

 private:
    std::atomic_bool is_acquired;
};
