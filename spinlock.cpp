//
// Created by gaporf on 30.03.2020.
//

#include "spinlock.hpp"

spinlock::spinlock() : is_acquired(false) {}

void spinlock::lock() {
    while (is_acquired.exchange(true, std::memory_order_acquire)) {}
}

bool spinlock::try_lock() {
    return !is_acquired.exchange(true, std::memory_order_consume);
}

void spinlock::unlock() {
    is_acquired.store(false, std::memory_order_release);
}
