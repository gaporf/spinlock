//
// Created by gaporf on 30.03.2020.
//

#include <gtest/gtest.h>
#include <spinlock.hpp>

#include <mutex>
#include <thread>
#include <algorithm>
#include <queue>
#include <condition_variable>

size_t const N = 1'000;
size_t const LOG2N = 10;

TEST(correctness, one_ownership) {
    spinlock sp;
    size_t count = 0;
    std::vector<std::thread> threads;
    for (size_t i = 0; i < N; i++) {
        threads.emplace_back([&sp, &count]() {
            std::lock_guard<spinlock> lg(sp);
            count++;
            ASSERT_TRUE(count == 1);
            count--;
        });
    }
    std::for_each(threads.begin(), threads.end(), [](auto &t) {
        t.join();
    });
}

TEST(correctness, thread_count) {
    spinlock sp;
    size_t count = 0;
    std::vector<std::thread> threads;
    for (size_t i = 0; i < N; i++) {
        threads.emplace_back([&sp, &count]() {
            std::lock_guard<spinlock> lg(sp);
            count++;
        });
    }
    std::for_each(threads.begin(), threads.end(), [](auto &t) {
        t.join();
    });
    EXPECT_EQ(N, count);
}

TEST(correctness, consumer_producer) {
    spinlock sp;
    std::condition_variable_any cv;
    std::queue<int> que;
    std::vector<std::thread> producers, consumers;
    for (size_t i = 0; i < N; i++) {
        consumers.emplace_back([&sp, &cv, &que]() {
            std::unique_lock<spinlock> lg(sp);
            if (que.empty()) {
                cv.wait(lg, [&que] { return !que.empty(); });
            }
            int v = que.front();
            que.pop();
            ASSERT_TRUE(v == 2);
        });
    }
    for (size_t i = 0; i < N; i++) {
        producers.emplace_back([&sp, &cv, &que, &i]() {
            std::unique_lock<spinlock> lg(sp);
            que.push(2);
            if (que.size() == 1) {
                cv.notify_all();
            }
        });
    }
    std::for_each(producers.begin(), producers.end(), [](auto &th) {
        th.join();
    });
    std::for_each(consumers.begin(), consumers.end(), [](auto &th) {
        th.join();
    });
    ASSERT_TRUE(que.empty());
}

TEST(correctness, without_deadlocks) {
    std::vector<spinlock> sps(LOG2N);
    for (size_t times = 0; times < LOG2N; times++) {
        std::vector<std::thread> th;
        for (size_t i = 0; i < N; i++) {
            th.emplace_back([&sps, i]() {
                for (size_t j = 0, k = 1; j < LOG2N; j++, k *= 2) {
                    if ((i & k) == 1) {
                        sps[j].lock();
                    }
                }
                for (size_t j = 0, k = 1; j < LOG2N; j++, k *= 2) {
                    if ((i & k) == 1) {
                        sps[j].unlock();
                    }
                }
            });
        }
        std::for_each(th.begin(), th.end(), [](auto &th) {
            th.join();
        });
    }
}

TEST(corretness, try_lock) {
    spinlock sp;
    ASSERT_TRUE(sp.try_lock());
    ASSERT_FALSE(sp.try_lock());
    sp.unlock();
    ASSERT_TRUE(sp.try_lock());
    sp.unlock();
}