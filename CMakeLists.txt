cmake_minimum_required(VERSION 3.15)

project(SPINLOCK)

include_directories(${SPINLOCK_SOURCE_DIR})

set(CMAKE_CXX_STANDARD 20)

add_library(spinlock
        spinlock.hpp
        spinlock.cpp)

add_executable(testing
        test.cpp
        gtest/gtest-all.cc
        gtest/gtest.h
        gtest/gtest_main.cc)

target_link_libraries(testing -lpthread)
target_link_libraries(testing spinlock)